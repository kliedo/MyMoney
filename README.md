# MyMoney


## 專案功能
- 提供一頁式(SPA)的網頁用來記帳。
- 前後端分離。
- 有機會就開發手機專用的 App 。
- 要有科目功能，會有借貸的會計原則出現。
- 要可以自動抓取發票，並且可以自動兌獎。
- 要有帳號密碼登入的功能，要有二階段認證的功能。
- 資料庫儲存的資料要有足夠的安全性。

# Git comment 規範
狀態分為以下幾種：
1.  feat: 新增/修改功能 (feature)。
2.  del: 移除功能或檔案。
3.  fix: 修補 bug (bug fix)。
4.  docs: 文件 (documentation)。
5.  style: 格式 (不影響程式碼運行的變動 white-space, formatting, missing semi colons, etc)。
6.  refactor: 重構 (既不是新增功能，也不是修補 bug 的程式碼變動)。
7.  perf: 改善效能 (A code change that improves performance)。
8.  test: 增加測試 (when adding missing tests)。
9.  chore: 建構程序或輔助工具的變動 (maintain)。
10. revert: 撤銷回覆先前的 commit 例如：revert: type(scope): subject (回覆版本：xxxx)。
11. tmp: 暫存，用於不同電腦間同步未完成的程式碼時使用。

git comment 內容格式如下：

「狀態」（必須，從上述狀態選擇最符合的狀態）：標題（必須，此comment的簡短描述，不要超過50字，結尾不句號）

問題：／新的需求：（二擇一，必須）

原因：（當上方是「問題」時，需要寫這個）

調整項目：（必須）

任務編號：（如有相關任務編號則為必填）
